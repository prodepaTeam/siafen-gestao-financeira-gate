import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ng2-webstorage';

import { GestaoFinanceiraSharedModule, UserRouteAccessService } from './shared';
import { GestaoFinanceiraHomeModule } from './home/home.module';
import { GestaoFinanceiraAdminModule } from './admin/admin.module';
import { GestaoFinanceiraAccountModule } from './account/account.module';
import { GestaoFinanceiraEntityModule } from './entities/entity.module';

import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';

import {
    JhiMainComponent,
    LayoutRoutingModule,
    NavbarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';

@NgModule({
    imports: [
        BrowserModule,
        LayoutRoutingModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        GestaoFinanceiraSharedModule,
        GestaoFinanceiraHomeModule,
        GestaoFinanceiraAdminModule,
        GestaoFinanceiraAccountModule,
        GestaoFinanceiraEntityModule
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class GestaoFinanceiraAppModule {}
