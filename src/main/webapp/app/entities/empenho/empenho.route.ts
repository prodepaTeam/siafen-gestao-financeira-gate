import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { EmpenhoComponent } from './empenho.component';
import { EmpenhoDetailComponent } from './empenho-detail.component';
import { EmpenhoPopupComponent } from './empenho-dialog.component';
import { EmpenhoDeletePopupComponent } from './empenho-delete-dialog.component';

import { Principal } from '../../shared';

export const empenhoRoute: Routes = [
    {
        path: 'empenho',
        component: EmpenhoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.empenho.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'empenho/:id',
        component: EmpenhoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.empenho.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const empenhoPopupRoute: Routes = [
    {
        path: 'empenho-new',
        component: EmpenhoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.empenho.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'empenho/:id/edit',
        component: EmpenhoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.empenho.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'empenho/:id/delete',
        component: EmpenhoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.empenho.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
