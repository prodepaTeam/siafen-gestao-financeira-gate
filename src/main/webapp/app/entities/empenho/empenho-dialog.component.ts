import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { Empenho } from './empenho.model';
import { EmpenhoPopupService } from './empenho-popup.service';
import { EmpenhoService } from './empenho.service';

@Component({
    selector: 'jhi-empenho-dialog',
    templateUrl: './empenho-dialog.component.html'
})
export class EmpenhoDialogComponent implements OnInit {

    empenho: Empenho;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private empenhoService: EmpenhoService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.empenho.id !== undefined) {
            this.subscribeToSaveResponse(
                this.empenhoService.update(this.empenho), false);
        } else {
            this.subscribeToSaveResponse(
                this.empenhoService.create(this.empenho), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<Empenho>, isCreated: boolean) {
        result.subscribe((res: Empenho) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Empenho, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'gestaoFinanceiraApp.empenho.created' : 'gestaoFinanceiraApp.empenho.updated',
            { param : result.id }
            , null);

        this.eventManager.broadcast({ name: 'empenhoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-empenho-popup',
    template: ''
})
export class EmpenhoPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private empenhoPopupService: EmpenhoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.empenhoPopupService
                    .open(EmpenhoDialogComponent, params['id']);
            } else {
                this.modalRef = this.empenhoPopupService
                    .open(EmpenhoDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
