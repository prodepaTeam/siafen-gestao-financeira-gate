import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Empenho } from './empenho.model';
import { EmpenhoService } from './empenho.service';

@Injectable()
export class EmpenhoPopupService {
    private isOpen = false;
    
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private empenhoService: EmpenhoService
    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.empenhoService.find(id).subscribe((empenho) => {
                this.empenhoModalRef(component, empenho);
            });
        } else {
            return this.empenhoModalRef(component, new Empenho());
        }
    }

    empenhoModalRef(component: Component, empenho: Empenho): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.empenho = empenho;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
