import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager  } from 'ng-jhipster';

import { Empenho } from './empenho.model';
import { EmpenhoService } from './empenho.service';

@Component({
    selector: 'jhi-empenho-detail',
    templateUrl: './empenho-detail.component.html'
})
export class EmpenhoDetailComponent implements OnInit, OnDestroy {

    empenho: Empenho;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: EventManager,
        private empenhoService: EmpenhoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEmpenhos();
    }

    load(id) {
        this.empenhoService.find(id).subscribe((empenho) => {
            console.log('>>>> ' + JSON.stringify(empenho));
            
            this.empenho = empenho;
        });
    }
    
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEmpenhos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'empenhoListModification',
            (response) => this.load(this.empenho.id)
        );
    }
}
