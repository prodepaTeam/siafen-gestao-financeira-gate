import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GestaoFinanceiraSharedModule } from '../../shared';
import {
    EmpenhoService,
    EmpenhoPopupService,
    EmpenhoComponent,
    EmpenhoDetailComponent,
    EmpenhoDialogComponent,
    EmpenhoPopupComponent,
    EmpenhoDeletePopupComponent,
    EmpenhoDeleteDialogComponent,
    empenhoRoute,
    empenhoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...empenhoRoute,
    ...empenhoPopupRoute,
];

@NgModule({
    imports: [
        GestaoFinanceiraSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EmpenhoComponent,
        EmpenhoDetailComponent,
        EmpenhoDialogComponent,
        EmpenhoDeleteDialogComponent,
        EmpenhoPopupComponent,
        EmpenhoDeletePopupComponent,
    ],
    entryComponents: [
        EmpenhoComponent,
        EmpenhoDialogComponent,
        EmpenhoPopupComponent,
        EmpenhoDeleteDialogComponent,
        EmpenhoDeletePopupComponent,
    ],
    providers: [
        EmpenhoService,
        EmpenhoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GestaoFinanceiraEmpenhoModule {}
