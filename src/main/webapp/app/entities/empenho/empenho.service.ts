import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Empenho } from './empenho.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EmpenhoService {
    private resourceUrl = 'api/empenhos';

    constructor(private http: Http) { }

    create(empenho: Empenho): Observable<Empenho> {
        const copy = this.convert(empenho);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(empenho: Empenho): Observable<Empenho> {
        const copy = this.convert(empenho);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Empenho> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(empenho: Empenho): Empenho {
        const copy: Empenho = Object.assign({}, empenho);
        return copy;
    }
}
