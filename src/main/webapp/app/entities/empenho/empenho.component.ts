import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { Empenho } from './empenho.model';
import { EmpenhoService } from './empenho.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-empenho',
    templateUrl: './empenho.component.html'
})
export class EmpenhoComponent implements OnInit, OnDestroy {
    empenhos: Empenho[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private empenhoService: EmpenhoService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.empenhoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.empenhos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEmpenhos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Empenho) {
        return item.id;
    }

    registerChangeInEmpenhos() {
        this.eventSubscriber = this.eventManager.subscribe('empenhoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
