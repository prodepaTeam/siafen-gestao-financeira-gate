import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { Empenho } from './empenho.model';
import { EmpenhoPopupService } from './empenho-popup.service';
import { EmpenhoService } from './empenho.service';

@Component({
    selector: 'jhi-empenho-delete-dialog',
    templateUrl: './empenho-delete-dialog.component.html'
})
export class EmpenhoDeleteDialogComponent {

    empenho: Empenho;

    constructor(
        private empenhoService: EmpenhoService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.empenhoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'empenhoListModification',
                content: 'Deleted an empenho'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('gestaoFinanceiraApp.empenho.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-empenho-delete-popup',
    template: ''
})
export class EmpenhoDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private empenhoPopupService: EmpenhoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.empenhoPopupService
                .open(EmpenhoDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
