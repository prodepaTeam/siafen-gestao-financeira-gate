export class Empenho {
    constructor(
        public id?: number,
        public numero?: string,
        public idUnidadeGestora?: string,
        public unidadeGestora?: string,
        public idGestao?: string,
        public gestao?: string,
        public valor?: string,
        public data?: Date
    ) {
    }
}
