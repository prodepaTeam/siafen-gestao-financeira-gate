import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService } from 'ng-jhipster';

import { DashboardItem } from './dashboard-item.model';
import { DashboardItemPopupService } from './dashboard-item-popup.service';
import { DashboardItemService } from './dashboard-item.service';

@Component({
    selector: 'jhi-dashboard-item-dialog',
    templateUrl: './dashboard-item-dialog.component.html'
})
export class DashboardItemDialogComponent implements OnInit {

    dashboardItem: DashboardItem;
    authorities: any[];
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private dashboardItemService: DashboardItemService,
        private eventManager: EventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dashboardItem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dashboardItemService.update(this.dashboardItem), false);
        } else {
            this.subscribeToSaveResponse(
                this.dashboardItemService.create(this.dashboardItem), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<DashboardItem>, isCreated: boolean) {
        result.subscribe((res: DashboardItem) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: DashboardItem, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'gestaoFinanceiraApp.dashboardItem.created'
            : 'gestaoFinanceiraApp.dashboardItem.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'dashboardItemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-dashboard-item-popup',
    template: ''
})
export class DashboardItemPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dashboardItemPopupService: DashboardItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.dashboardItemPopupService
                    .open(DashboardItemDialogComponent, params['id']);
            } else {
                this.modalRef = this.dashboardItemPopupService
                    .open(DashboardItemDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
