import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { DashboardItemComponent } from './dashboard-item.component';
import { DashboardItemDetailComponent } from './dashboard-item-detail.component';
import { DashboardItemPopupComponent } from './dashboard-item-dialog.component';
import { DashboardItemDeletePopupComponent } from './dashboard-item-delete-dialog.component';

import { Principal } from '../../shared';

export const dashboardItemRoute: Routes = [
    {
        path: 'dashboard-item',
        component: DashboardItemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.dashboardItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'dashboard-item/:id',
        component: DashboardItemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.dashboardItem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dashboardItemPopupRoute: Routes = [
    {
        path: 'dashboard-item-new',
        component: DashboardItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.dashboardItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dashboard-item/:id/edit',
        component: DashboardItemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.dashboardItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'dashboard-item/:id/delete',
        component: DashboardItemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gestaoFinanceiraApp.dashboardItem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
