import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { DashboardItem } from './dashboard-item.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class DashboardItemService {

    private resourceUrl = 'api/dashboard-items';
    // private resourceUrl = 'api/dashboard';

    constructor(private http: Http) { }

    create(dashboardItem: DashboardItem): Observable<DashboardItem> {
        const copy = this.convert(dashboardItem);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(dashboardItem: DashboardItem): Observable<DashboardItem> {
        const copy = this.convert(dashboardItem);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<DashboardItem> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(dashboardItem: DashboardItem): DashboardItem {
        const copy: DashboardItem = Object.assign({}, dashboardItem);
        return copy;
    }
}
