import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, EventManager } from 'ng-jhipster';

import { DashboardItem } from './dashboard-item.model';
import { DashboardItemPopupService } from './dashboard-item-popup.service';
import { DashboardItemService } from './dashboard-item.service';

@Component({
    selector: 'jhi-dashboard-item-delete-dialog',
    templateUrl: './dashboard-item-delete-dialog.component.html'
})
export class DashboardItemDeleteDialogComponent {

    dashboardItem: DashboardItem;

    constructor(
        private dashboardItemService: DashboardItemService,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private eventManager: EventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.dashboardItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dashboardItemListModification',
                content: 'Deleted an dashboardItem'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('gestaoFinanceiraApp.dashboardItem.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-dashboard-item-delete-popup',
    template: ''
})
export class DashboardItemDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dashboardItemPopupService: DashboardItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.dashboardItemPopupService
                .open(DashboardItemDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
