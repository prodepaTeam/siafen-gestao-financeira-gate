import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DashboardItem } from './dashboard-item.model';
import { DashboardItemService } from './dashboard-item.service';
@Injectable()
export class DashboardItemPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private dashboardItemService: DashboardItemService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.dashboardItemService.find(id).subscribe((dashboardItem) => {
                this.dashboardItemModalRef(component, dashboardItem);
            });
        } else {
            return this.dashboardItemModalRef(component, new DashboardItem());
        }
    }

    dashboardItemModalRef(component: Component, dashboardItem: DashboardItem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dashboardItem = dashboardItem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
