export class DashboardItem {
    constructor(
        public id?: number,
        public name?: string,
        public shortName?: string,
        public description?: string,
        public order?: number,
        public iconName?: string,
        public iconColor?: string,
        public backgroundColor?: string,
    ) {
    }
}
